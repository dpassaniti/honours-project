import matplotlib.pyplot as plt
import numpy as np
 
#render
renderData = [24.844,20.1293,32.665,38.2417,36.011,36.1069,31.1291,26.77,32.2064,33.5779,34.292,34.0279,33.2506,33.4359,31.6184,32.4174,33.9709,33.2534,32.4782,34.4128,35.2645,26.1631,32.8973,36.3878,35.0267,34.9011,33.7888,32.7086,33.7804,29.83,33.1513,34.8257,36.2786,30.9526,37.3628,36.3383,33.0496,15.1298,2.50797,2.33448,2.38022,2.43832,10.1543,40.2145,66.0044,63.0862]
render = 0
for i in range(0,len(renderData)):
	render += renderData[i]
render /= len(renderData)

#render stationary
renderDataS = [46.3891,46.5086,46.526,46.511,46.9228,46.0274,46.9772,46.0905,46.9651,46.5397,46.0915,46.9365,46.1842,47.0632,46.5331,46.0702,46.4934,46.4056,46.557,47.4656,45.6251,47.4674,45.6648,47.5228]
renderS = 0
for i in range(0,len(renderDataS)):
	renderS += renderDataS[i]
renderS /= len(renderDataS)

#compute
computeData = [0.124346,0.13346,0.0962884,0.107038,0.109408,0.10289,0.111502,0.103708,0.0950835,0.109357,0.103447,0.0956839,0.116674,0.101227,0.098398,0.119601,0.10372,0.102455,0.121043,0.12467,0.121126,0.103285,0.10972,0.101847,0.108105,0.0996425,0.0980069,0.109088,0.103032,0.0995872,0.108464]

compute = 0
for i in range(0,len(computeData)):
	compute += computeData[i]
compute /= len(computeData)

#compute stationary
computeDataS = [0.121624,0.122402,0.128778,0.114825,0.12941,0.117547,0.112134,0.124492,0.11712,0.112391,0.12484,0.118258,0.118613,0.136198,0.131879,0.11437,0.132476,0.133705,0.125294,0.128269,0.119621,0.107943,0.127396,0.118293,0.113853,0.119313,0.119095,0.119814] 

computeS = 0
for i in range(0,len(computeDataS)):
	computeS += computeDataS[i]
computeS /= len(computeDataS)

#frame
frameData = [42.4008,14.2605,14.7979,15.8953,24.9958,38.0811,34.609,31.0662,34.6102,36.2177,39.0876,33.2796,35.9901,35.2251,35.9608,36.5078,32.231,30.1893,33.883,33.6288,35.9518,37.7087,35.3073,36.4256,34.6639,31.2859,35.6593,38.4013,41.6099,39.6447,2.82462,2.90595,2.8894,2.91483,12.4949,34.9099,34.5137,38.727,35.5881,29.0375,26.4669,27.7214,43.9751,92.1944,98.8067]
frame = 0
for i in range(0,len(frameData)):
	frame += frameData[i]
frame /= len(frameData)

#frame stationary
frameDataS = [47.4359,45.6525,46.5285,47.4578,45.7151,46.4797,47.4871,45.5679,46.6184,46.548,46.6182,46.7005,46.4813,46.6669,47.6637,45.7517,47.5234,45.7483,47.5369]
frameS = 0
for i in range(0,len(frameDataS)):
	frameS += frameDataS[i]
frameS /= len(frameDataS)

#graph
groups = 3
performance = [frame, compute, render]
performanceS = [frameS, computeS, renderS]
objects = ("[{:.2f}]".format(frame)+'\n\nFrame\n\n'+'[{:.2f}]'.format(frameS), '[{:.2f}]'.format(compute)+'\n\nCollision\n\n'+'[{:.2f}]'.format(computeS),'[{:.2f}]'.format(render)+'\n\nDraw\n\n'+'[{:.2f}]'.format(renderS))
index = np.arange(groups)
barwidth = 0.3
moving = plt.barh(index+barwidth+barwidth/2, performance, barwidth, alpha = 0.6, color = '#0099ff', label = 'moving')
static = plt.barh(index+barwidth/2, performanceS, barwidth, alpha = 0.6, color = '#e64d00', label = 'static')
plt.xlabel('milliseconds')
plt.title('Architecture scene frame time')
plt.yticks(index + barwidth, objects)
axes = plt.gca()
axes.set_xlim([0,50])
plt.legend()
plt.show()
