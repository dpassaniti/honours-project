Scenes rendered by sphere tracing procedural distance fields, using Vulkan graphics API.

- Hons: c++ code for the 3 scenes Architecture, Fractal and Test Room
- shaders: glsl code for vertex, compute and fragment shaders
- graphs: python code for data visualisations used in the paper

Youtube videos:

- https://youtu.be/E6VTrF1rU8o
- https://youtu.be/dElx6Na-AgA