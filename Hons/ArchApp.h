#ifndef _ARCH_APP_H_
#define _ARCH_APP_H_
#include "BaseApplication.h"
#include <chrono>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Camera.h"

class ArchApp: public BaseApplication
{

public:
	ArchApp();
	~ArchApp();

	void run();

private:
	static const int UNIFORM_COUNT = 2;
	static const int TEX_COUNT = 4;

	std::vector<Vertex> vertices_ =
	{
		{ { 0.0f, sHEIGHT, 0.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { sWIDTH, sHEIGHT, 0.0f },{ 0.0f, 1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ { sWIDTH, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 0.0f }, },
		{ { 0.0f, 0.0f, 0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0f, 0.0f } }
	};
	const std::vector<uint16_t> indices_ = { 0, 1, 2, 2, 3, 0 };

	//TODO mul these and send just 1
	struct mvpUniform
	{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;
	};

	struct cameraUniform
	{
		glm::vec4 position;
		glm::vec4 up;
		glm::vec4 forward;
		glm::vec4 right;

		glm::vec2 screenSize;
		float time;

	};
	float time_;
	
	//scenevars
	Camera camera_;
	float frameTime_;

	struct textureStruct
	{
		std::array<const char*, TEX_COUNT> path;
		std::array<VkImage, TEX_COUNT> image;
		std::array<VkDeviceMemory, TEX_COUNT> memory;
		std::array<VkImageView, TEX_COUNT> imageView;
	} textures_;

	VkSampler textureSampler_;
	
	VkBuffer cameraBuffer_;
	VkDeviceMemory cameraBufferMemory_;
	VkBuffer cameraStagingBuffer_;
	VkDeviceMemory cameraStagingBufferMemory_;

	//core
	void initWindow();
	void initVulkan();
	void drawFrame();
	void mainLoop();
	void destroyVulkan();
	void handleInput();
	
	void recreateSwapchain();
	void createGraphicsPipeline();

	static void onWindowResized(GLFWwindow* window, int width, int height)
	{
		if (width == 0 || height == 0) return;//minimized

		ArchApp* app = reinterpret_cast<ArchApp*>(glfwGetWindowUserPointer(window));
		app->recreateSwapchain();
	}
	
	//texturing
	void createTextureImage();
	void createTextureImageView();
	void createTextureSampler();
	
	//uniforms, buffers and command queues
	void createVertexBuffer();
	void createIndexBuffer();
	void createUniformBuffers();
	void updateUniformBuffers();
	void createDescriptorSetLayout();
	void createDescriptorSet();
	void createCommandPools();
	void createCommandBuffers();
	void createDescriptorPool();

	//compute
	struct collisionInfo
	{
		glm::vec4 camPos;
		float time;
		bool result;
	};

	struct computePipeline
	{
		VkQueue queue;
		VkCommandPool commandPool;
		VkCommandBuffer cmdBfr;
		VkPipeline pipeline;
		VkPipelineLayout pipelineLayout;
		VkDescriptorSet descriptorSet;
		VkDescriptorSetLayout descriptorSetLayout;

		VkDeviceMemory collisionDataBfrMemory;
		VkBuffer collisionDataBfr;
		collisionInfo collisionData;

	} compute_;

	void createLogicalDevice();//overload to add compute queue
	void createComputePipeline();
	void createComputeDescriptorSetLayout();
	void createComputeDescriptorSet();
	void createComputeBuffers();
	bool checkCollisions();
};

#endif

