#ifndef _CAMERA_H_
#define _CAMERA_H_

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

class Camera
{

public:
	Camera();
	~Camera();

	void update(float frameTime);

	void moveForward();
	void moveBackward();
	void moveUpward();
	void moveDownward();
	void turnLeft();
	void turnRight();
	void turnUp();
	void turnDown();
	void strafeRight();
	void strafeLeft();

	//setters
	void setPosition(glm::vec4 pos) {position_ = pos;};
	void setRotation(glm::vec4 rot) {rotation_ = rot;};

	//getters
	glm::vec4 position() {return position_;};
	glm::vec4 up() {return up_;};
	glm::vec4 forward() {return forward_;};
	glm::vec4 right() {return right_;};

private:
	glm::vec4 position_;
	glm::vec4 rotation_;
	glm::vec4 up_, lookAt_, forward_, right_;
	float speed_, rotationSpeed_, frameTime_;
};

#endif//_CAMERA_H_
