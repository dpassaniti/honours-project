#include "ArchApp.h"

ArchApp::ArchApp()
	: BaseApplication()
	, frameTime_(0)
	, time_(0)
{
	//textures
	textures_.path[0] = "../textures/marble3.jpg";
	textures_.path[1] = "../textures/flower.jpg";
	textures_.path[2] = "../textures/carving.jpg";
	textures_.path[3] = "../textures/sky.jpg";

	camera_.setPosition(glm::vec4(300.0f, 100.0f, 300.0f, 1.0f));
	camera_.setRotation(glm::vec4(-20.0f, 45.0f, 0.0f, 0.0f));
}

ArchApp::~ArchApp()
{
	destroyVulkan();
}

void ArchApp::handleInput()
{
	int state;

	state = glfwGetKey(window_, GLFW_KEY_E);
	if (state == GLFW_PRESS) camera_.moveUpward();

	state = glfwGetKey(window_, GLFW_KEY_Q);
	if (state == GLFW_PRESS) camera_.moveDownward();

	state = glfwGetKey(window_, GLFW_KEY_W);
	if (state == GLFW_PRESS) camera_.moveForward();

	state = glfwGetKey(window_, GLFW_KEY_A);
	if (state == GLFW_PRESS) camera_.strafeLeft();

	state = glfwGetKey(window_, GLFW_KEY_S);
	if (state == GLFW_PRESS) camera_.moveBackward();

	state = glfwGetKey(window_, GLFW_KEY_D);
	if (state == GLFW_PRESS) camera_.strafeRight();

	state = glfwGetKey(window_, GLFW_KEY_UP);
	if (state == GLFW_PRESS) camera_.turnUp();

	state = glfwGetKey(window_, GLFW_KEY_DOWN);
	if (state == GLFW_PRESS) camera_.turnDown();

	state = glfwGetKey(window_, GLFW_KEY_LEFT);
	if (state == GLFW_PRESS) camera_.turnLeft();

	state = glfwGetKey(window_, GLFW_KEY_RIGHT);
	if (state == GLFW_PRESS) camera_.turnRight();

	state = glfwGetKey(window_, GLFW_KEY_ESCAPE);
	if (state == GLFW_PRESS) glfwSetWindowShouldClose(window_, true);
}

void ArchApp::initWindow()
{
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);//don't init OpenGL

	window_ = glfwCreateWindow(width_, height_, "Architecture scene", nullptr, nullptr);

	glfwSetWindowUserPointer(window_, this);
	glfwSetWindowSizeCallback(window_, onWindowResized);
}

void ArchApp::initVulkan()
{
	createInstance();
	setupDebugCallback();
	createSurface();
	pickPhysicalDevice();
	createLogicalDevice();
	createSwapchain();
	createImageViews();
	createRenderPass();
	createDescriptorSetLayout();
	createComputeDescriptorSetLayout();
	createGraphicsPipeline();
	createComputePipeline();
	createFramebuffers();
	createCommandPools();
	createTextureImage();
	createTextureImageView();
	createTextureSampler();
	createVertexBuffer();
	createIndexBuffer();
	createUniformBuffers();
	createComputeBuffers();
	createDescriptorPool();
	createComputeDescriptorSet();
	createDescriptorSet();
	createCommandBuffers();
	createSemaphores();
}

void ArchApp::drawFrame()
{
	uint32_t imageIndex;
	VkResult result = vkAcquireNextImageKHR(device_, swapchain_, std::numeric_limits<uint64_t>::max(),
		imageAvailableSemaphore_, VK_NULL_HANDLE, &imageIndex);
	//if swapchain out of date, recreate it and return
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		recreateSwapchain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
		throw std::runtime_error("failed to acquire swap chain image");

	VkSemaphore waitSemaphores[] = { imageAvailableSemaphore_ };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphore_ };
	VkSwapchainKHR swapchains[] = { swapchain_ };

	//submit info
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers_[imageIndex];
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	if (vkQueueSubmit(graphicsQueue_, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
		throw std::runtime_error("failed to submit draw command buffer");

	//present info
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapchains;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr; // Optional

	result = vkQueuePresentKHR(presentQueue_, &presentInfo);
	//if swapchain out of date, recreate it
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
		recreateSwapchain();
	else if (result != VK_SUCCESS)
		throw std::runtime_error("failed to present swap chain image");
}

void ArchApp::mainLoop()
{
	//INIT_TIME
	while (!glfwWindowShouldClose(window_))
	{
		//START_TIME
		auto startTime = std::chrono::high_resolution_clock::now();

		//update
		glfwPollEvents();

		glm::vec4 lastPos = camera_.position();
		handleInput();
		
		//collision detection
		if (checkCollisions()) camera_.setPosition(lastPos);
		camera_.update(frameTime_);
		updateUniformBuffers();

		//render
		drawFrame();

		auto currentTime = std::chrono::high_resolution_clock::now();
		frameTime_ = std::chrono::duration<float>(currentTime - startTime).count();

		time_ += frameTime_;
		//STOP_TIME
		//PRINT_TIME
	}

	//make sure all threads are done before quitting
	vkDeviceWaitIdle(device_);
}

void ArchApp::destroyVulkan()
{
	//destroy order matteres

	//synchronisation
	vkDestroySemaphore(device_, renderFinishedSemaphore_, nullptr);
	vkDestroySemaphore(device_, imageAvailableSemaphore_, nullptr);

	//buffers
	vkFreeMemory(device_, compute_.collisionDataBfrMemory, nullptr);
	vkDestroyBuffer(device_, compute_.collisionDataBfr, nullptr);
	vkFreeMemory(device_, indexBufferMemory_, nullptr);
	vkDestroyBuffer(device_, indexBuffer_, nullptr);
	vkFreeMemory(device_, vertexBufferMemory_, nullptr);
	vkDestroyBuffer(device_, vertexBuffer_, nullptr);

	//uniform buffers
	vkFreeMemory(device_, mvpBufferMemory_, nullptr);
	vkDestroyBuffer(device_, mvpBuffer_, nullptr);
	vkFreeMemory(device_, mvpStagingBufferMemory_, nullptr);
	vkDestroyBuffer(device_, mvpStagingBuffer_, nullptr);

	vkFreeMemory(device_, cameraBufferMemory_, nullptr);
	vkDestroyBuffer(device_, cameraBuffer_, nullptr);
	vkFreeMemory(device_, cameraStagingBufferMemory_, nullptr);
	vkDestroyBuffer(device_, cameraStagingBuffer_, nullptr);

	//textures
	vkDestroySampler(device_, textureSampler_, nullptr);
	for (int i = 0; i < TEX_COUNT; ++i)
	{
		vkDestroyImageView(device_, textures_.imageView[i], nullptr);
		vkFreeMemory(device_, textures_.memory[i], nullptr);
		vkDestroyImage(device_, textures_.image[i], nullptr);
	}

	vkDestroyDescriptorPool(device_, descriptorPool_, nullptr);
	//compute pipeline
	vkDestroyCommandPool(device_, compute_.commandPool, nullptr);
	vkDestroyPipeline(device_, compute_.pipeline, nullptr);
	vkDestroyPipelineLayout(device_, compute_.pipelineLayout, nullptr);
	vkDestroyDescriptorSetLayout(device_, compute_.descriptorSetLayout, nullptr);

	//graphics pipeline
	vkDestroyCommandPool(device_, commandPool_, nullptr);
	for (VkFramebuffer& fb : swapchainFramebuffers_)
		vkDestroyFramebuffer(device_, fb, nullptr);
	vkDestroyPipeline(device_, graphicsPipeline_, nullptr);
	vkDestroyPipelineLayout(device_, pipelineLayout_, nullptr);
	vkDestroyRenderPass(device_, renderPass_, nullptr);
	vkDestroyDescriptorSetLayout(device_, descriptorSetLayout_, nullptr);
	for (VkImageView& iv : swapchainImageViews_)
		vkDestroyImageView(device_, iv, nullptr);
	vkDestroySwapchainKHR(device_, swapchain_, nullptr);

	//
	vkDestroySurfaceKHR(instance_, surface_, nullptr);
	vkDestroyDevice(device_, nullptr);

	//debug callback
	if (enableValidationLayers)
	{
		//load function ptr and use it
		PFN_vkDestroyDebugReportCallbackEXT func =
			(PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance_, "vkDestroyDebugReportCallbackEXT");
		func(instance_, callback_, nullptr);
	}

	//
	vkDestroyInstance(instance_, nullptr);
}

void ArchApp::run()
{
	initWindow();
	initVulkan();
	mainLoop();
}

void ArchApp::createComputePipeline()
{
	//SHADER STAGE
	VkShaderModule compShaderModule;
	std::vector<char> compShaderCode = readFile("../shaders/collision.comp.spv");
	createShaderModule(compShaderCode, compShaderModule);

	VkPipelineShaderStageCreateInfo compShaderStageInfo = {};
	compShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	compShaderStageInfo.pNext = nullptr;
	compShaderStageInfo.flags = 0;
	compShaderStageInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
	compShaderStageInfo.module = compShaderModule;
	compShaderStageInfo.pName = "main";
	compShaderStageInfo.pSpecializationInfo = nullptr;

	//PIPELINE LAYOUT
	VkDescriptorSetLayout setLayouts[] = { compute_.descriptorSetLayout };
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = setLayouts;
	pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
	pipelineLayoutInfo.pPushConstantRanges = 0; // Optional

	if (vkCreatePipelineLayout(device_, &pipelineLayoutInfo, nullptr, &compute_.pipelineLayout) != VK_SUCCESS)
		throw std::runtime_error("failed to create compute pipeline layout");

	//PIPELINE
	VkComputePipelineCreateInfo pipelineInfo;
	pipelineInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	pipelineInfo.pNext = nullptr;
	pipelineInfo.flags = 0;
	pipelineInfo.stage = compShaderStageInfo;
	pipelineInfo.layout = compute_.pipelineLayout;
	pipelineInfo.basePipelineHandle = 0;
	pipelineInfo.basePipelineIndex = 0;

	if (vkCreateComputePipelines(device_, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &compute_.pipeline) != VK_SUCCESS)
		throw std::runtime_error("failed to create compute pipeline");

	vkDestroyShaderModule(device_, compShaderModule, nullptr);
}

void ArchApp::createCommandPools()
{
	QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice_);

	//compute
	VkCommandPoolCreateInfo computePoolInfo = {};
	computePoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	computePoolInfo.queueFamilyIndex = queueFamilyIndices.computeFamily;
	computePoolInfo.flags = 0; // Optional

	if (vkCreateCommandPool(device_, &computePoolInfo, nullptr, &compute_.commandPool) != VK_SUCCESS)
		throw std::runtime_error("failed to create compute command pool");

	//graphics
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	poolInfo.flags = 0; // Optional

	if (vkCreateCommandPool(device_, &poolInfo, nullptr, &commandPool_) != VK_SUCCESS)
		throw std::runtime_error("failed to create command pool");
}

void ArchApp::createCommandBuffers()
{
	//no need to clear command buffers at end of program since they are automatically destroyed whith their command pool,
	//however when recreating the swapchain we do not recreate cmd pool, so we need to check and destroy in that case
	if (commandBuffers_.size() > 0)
		vkFreeCommandBuffers(device_, commandPool_, commandBuffers_.size(), commandBuffers_.data());

	//need 1 cmd buff for each framebuffer
	commandBuffers_.resize(swapchainFramebuffers_.size());
	VkCommandBufferAllocateInfo allocInfo = {};

	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool_;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)commandBuffers_.size();

	if (vkAllocateCommandBuffers(device_, &allocInfo, commandBuffers_.data()) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate command buffers");

	for (size_t i = 0; i < commandBuffers_.size(); i++)
	{
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		beginInfo.pInheritanceInfo = nullptr; // Optional

		vkBeginCommandBuffer(commandBuffers_[i], &beginInfo);

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = renderPass_;
		renderPassInfo.framebuffer = swapchainFramebuffers_[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapchainExtent_;
		VkClearValue clearColor = { 0.2f, 0.2f, 0.2f, 1.0f };
		renderPassInfo.clearValueCount = 1;
		renderPassInfo.pClearValues = &clearColor;

		//record commands
		vkCmdBeginRenderPass(commandBuffers_[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
		vkCmdBindPipeline(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline_);

		//bind buffers
		VkBuffer vertexBuffers[] = { vertexBuffer_ };
		VkDeviceSize offsets[] = { 0 };
		vkCmdBindVertexBuffers(commandBuffers_[i], 0, 1, vertexBuffers, offsets);
		vkCmdBindIndexBuffer(commandBuffers_[i], indexBuffer_, 0, VK_INDEX_TYPE_UINT16);

		//uniforms
		vkCmdBindDescriptorSets(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout_, 0, 1, &descriptorSet_, 0, nullptr);

		vkCmdDrawIndexed(commandBuffers_[i], indices_.size(), 1, 0, 0, 0);
		vkCmdEndRenderPass(commandBuffers_[i]);
		if (vkEndCommandBuffer(commandBuffers_[i]) != VK_SUCCESS)
			throw std::runtime_error("failed to record command buffer");
	}

	//COMPUTE COMMAND BUFFERS
	//init compute command buffer
	VkCommandBufferAllocateInfo allocInfoCompute = {};
	allocInfoCompute.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfoCompute.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfoCompute.commandPool = compute_.commandPool;
	allocInfoCompute.commandBufferCount = 1;
	if (vkAllocateCommandBuffers(device_, &allocInfoCompute, &compute_.cmdBfr) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate compute command buffer");

	//record commands
	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

	vkBeginCommandBuffer(compute_.cmdBfr, &beginInfo);
	vkCmdBindPipeline(compute_.cmdBfr, VK_PIPELINE_BIND_POINT_COMPUTE, compute_.pipeline);
	vkCmdBindDescriptorSets(compute_.cmdBfr, VK_PIPELINE_BIND_POINT_COMPUTE, compute_.pipelineLayout, 0, 1, &compute_.descriptorSet, 0, nullptr);
	//TODO workgroup size
	vkCmdDispatch(compute_.cmdBfr, 1, 1, 1);
	if (vkEndCommandBuffer(compute_.cmdBfr) != VK_SUCCESS)
		throw std::runtime_error("failed to record compute command buffer");
}

void ArchApp::createUniformBuffers()
{
	//mvp
	VkDeviceSize bufferSize = sizeof(mvpUniform);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		mvpStagingBuffer_, mvpStagingBufferMemory_);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mvpBuffer_, mvpBufferMemory_);
	//initialise here as it won't change
	mvpUniform mvp = {};
	mvp.model = glm::mat4();
	mvp.view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	mvp.proj = glm::ortho(0.0f, (float)width_, (float)height_, 0.0f, .0f, 5.0f);
	void* mvpdata;
	vkMapMemory(device_, mvpStagingBufferMemory_, 0, sizeof(mvp), 0, &mvpdata);
	memcpy(mvpdata, &mvp, sizeof(mvp));
	vkUnmapMemory(device_, mvpStagingBufferMemory_);
	copyBuffer(mvpStagingBuffer_, mvpBuffer_, sizeof(mvp));

	//camera
	bufferSize = sizeof(cameraUniform);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		cameraStagingBuffer_, cameraStagingBufferMemory_);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, cameraBuffer_, cameraBufferMemory_);

}


void ArchApp::updateUniformBuffers()
{
	//camera
	cameraUniform cam = {};
	cam.position = camera_.position();
	cam.up = camera_.up();
	cam.forward = camera_.forward();
	cam.right = camera_.right();
	//TODO rename camera uniform to sceneData
	cam.time = time_;
	cam.screenSize.x = width_;
	cam.screenSize.y = width_;

	//TODO should use push constants for frequently changing values?
	void* data;
	vkMapMemory(device_, cameraStagingBufferMemory_, 0, sizeof(cam), 0, &data);
	memcpy(data, &cam, sizeof(cam));
	vkUnmapMemory(device_, cameraStagingBufferMemory_);
	copyBuffer(cameraStagingBuffer_, cameraBuffer_, sizeof(cam));
}

void ArchApp::createDescriptorPool()
{
	std::array<VkDescriptorPoolSize, 3> poolSizes = {};
	//graphics: x2 uniforms, x2 combined tex sampler
	//compute: x1 storage buffer
	poolSizes[0] = { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, UNIFORM_COUNT };
	poolSizes[1] = { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, TEX_COUNT };
	poolSizes[2] = { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1 };

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.maxSets = 2;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();

	if (vkCreateDescriptorPool(device_, &poolInfo, nullptr, &descriptorPool_) != VK_SUCCESS)
		throw std::runtime_error("failed to create descriptor pool");
}

void ArchApp::createDescriptorSet()
{
	VkDescriptorSetLayout layouts[] = { descriptorSetLayout_ };
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool_;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = layouts;

	if (vkAllocateDescriptorSets(device_, &allocInfo, &descriptorSet_) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate descriptor set");

	std::array<VkWriteDescriptorSet, UNIFORM_COUNT + TEX_COUNT> descriptorWrites = {};
	VkDescriptorBufferInfo bufferInfo[2];

	//mvp matrix buffer
	bufferInfo[0].buffer = mvpBuffer_;
	bufferInfo[0].offset = 0;
	bufferInfo[0].range = sizeof(mvpUniform);

	descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[0].dstSet = descriptorSet_;
	descriptorWrites[0].dstBinding = 0;
	descriptorWrites[0].dstArrayElement = 0;
	descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[0].descriptorCount = 1;
	descriptorWrites[0].pBufferInfo = &bufferInfo[0];
	descriptorWrites[0].pImageInfo = nullptr; // Optional
	descriptorWrites[0].pTexelBufferView = nullptr; // Optional

	//camera buffer
	bufferInfo[1].buffer = cameraBuffer_;
	bufferInfo[1].offset = 0;
	bufferInfo[1].range = sizeof(cameraUniform);

	descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[1].dstSet = descriptorSet_;
	descriptorWrites[1].dstBinding = 1;
	descriptorWrites[1].dstArrayElement = 0;
	descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[1].descriptorCount = 1;
	descriptorWrites[1].pBufferInfo = &bufferInfo[1];
	descriptorWrites[1].pImageInfo = nullptr; // Optional
	descriptorWrites[1].pTexelBufferView = nullptr; // Optional

	//textures
	std::array<VkDescriptorImageInfo, TEX_COUNT> imageInfo;
	for (int i = 0; i < TEX_COUNT; ++i)
	{
		imageInfo[i].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo[i].imageView = textures_.imageView[i];
		imageInfo[i].sampler = textureSampler_;
	}

	for (int i = UNIFORM_COUNT; i < UNIFORM_COUNT + TEX_COUNT; ++i)
	{
		descriptorWrites[i].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[i].dstSet = descriptorSet_;
		descriptorWrites[i].dstBinding = i;
		descriptorWrites[i].dstArrayElement = 0;
		descriptorWrites[i].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptorWrites[i].descriptorCount = 1;
		descriptorWrites[i].pImageInfo = &imageInfo[i - UNIFORM_COUNT];
	}

	vkUpdateDescriptorSets(device_, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
}

void ArchApp::createComputeDescriptorSetLayout()
{
	VkDescriptorSetLayoutBinding uboLayoutBinding[] =
	{
		{ 0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_COMPUTE_BIT, nullptr },
	};

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = 1;
	layoutInfo.pBindings = uboLayoutBinding;

	if (vkCreateDescriptorSetLayout(device_, &layoutInfo, nullptr, &compute_.descriptorSetLayout) != VK_SUCCESS)
		throw std::runtime_error("failed to create compute descriptor set layout");
}

void ArchApp::createComputeDescriptorSet()
{
	VkDescriptorSetLayout layouts[] = { compute_.descriptorSetLayout };

	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool_;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = layouts;

	if (vkAllocateDescriptorSets(device_, &allocInfo, &compute_.descriptorSet) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate compute descriptor set");

	VkDescriptorBufferInfo bufferInfo[] =
	{
		{ compute_.collisionDataBfr , 0, sizeof(collisionInfo) }
	};

	VkWriteDescriptorSet descriptorWrite[] =
	{
		{
			VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			nullptr,
			compute_.descriptorSet,
			0,
			0,
			1,
			VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
			nullptr,
			&bufferInfo[0],
			nullptr
		}
	};

	vkUpdateDescriptorSets(device_, 1, &descriptorWrite[0], 0, nullptr);
}

void ArchApp::createComputeBuffers()
{
	createBuffer(sizeof(collisionInfo),
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		compute_.collisionDataBfr, compute_.collisionDataBfrMemory);
}

bool ArchApp::checkCollisions()
{
	compute_.collisionData.camPos = camera_.position();
	compute_.collisionData.time = time_;
	compute_.collisionData.result = false;

	//TODO use FENCE to make sure compute is done if more than 1 workgroup?
	//copy collision info in device memory
	void* data;
	vkMapMemory(device_, compute_.collisionDataBfrMemory, 0, sizeof(collisionInfo), 0, &data);
	memcpy(data, &compute_.collisionData, sizeof(collisionInfo));

	//submit command buffer to compute queue
	VkSubmitInfo computeSubmitInfo;
	computeSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	computeSubmitInfo.pNext = nullptr;
	computeSubmitInfo.waitSemaphoreCount = 0;
	computeSubmitInfo.pWaitSemaphores = nullptr;
	computeSubmitInfo.pWaitDstStageMask = nullptr;
	computeSubmitInfo.commandBufferCount = 1;
	computeSubmitInfo.pCommandBuffers = &compute_.cmdBfr;
	computeSubmitInfo.signalSemaphoreCount = 0;
	computeSubmitInfo.pSignalSemaphores = nullptr;//&compute_.semaphore;
	vkQueueSubmit(compute_.queue, 1, &computeSubmitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(compute_.queue);

	//update collision data from device memory
	memcpy(&compute_.collisionData, data, sizeof(collisionInfo));
	vkUnmapMemory(device_, compute_.collisionDataBfrMemory);

	//	std::cout <<compute_.collisionData.result << " ";
	//return collision check result
	return compute_.collisionData.result;
}

void ArchApp::createLogicalDevice()
{
	//get appropriate queues
	//note: same queue might support both graphics and present etc., so use set
	QueueFamilyIndices indices = findQueueFamilies(physicalDevice_);
	std::vector<VkDeviceQueueCreateInfo> qinfos;
	std::set<int> uniqueQueueFamilies = { indices.graphicsFamily, indices.presentFamily, indices.computeFamily };
	float queuePriority = 1.0f;
	for (int queueFamily : uniqueQueueFamilies)
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		qinfos.push_back(queueCreateInfo);
	}

	//device info
	VkPhysicalDeviceFeatures deviceFeatures = {};//TODO specify used device features
	//deviceFeatures.fillModeNonSolid = true;

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pQueueCreateInfos = qinfos.data();
	createInfo.queueCreateInfoCount = (uint32_t)qinfos.size();
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledExtensionCount = deviceExtensions.size();
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();
	//TODO layers in logical device deprecated?
	if (enableValidationLayers)
	{
		createInfo.enabledLayerCount = validationLayers.size();
		createInfo.ppEnabledLayerNames = validationLayers.data();
	}
	else
		createInfo.enabledLayerCount = 0;

	if (vkCreateDevice(physicalDevice_, &createInfo, nullptr, &device_) != VK_SUCCESS)
		throw std::runtime_error("failed to create logical device!");

	vkGetDeviceQueue(device_, indices.graphicsFamily, 0, &graphicsQueue_);
	vkGetDeviceQueue(device_, indices.presentFamily, 0, &presentQueue_);
	vkGetDeviceQueue(device_, indices.computeFamily, 0, &compute_.queue);
}

void ArchApp::recreateSwapchain()
{
	//wait for threads
	vkDeviceWaitIdle(device_);

	//destroy affected vk resources
	//note: createSwapchain() already handles old swapchain destruction internally
	for (VkFramebuffer& fb : swapchainFramebuffers_)
		vkDestroyFramebuffer(device_, fb, nullptr);
	vkDestroyPipeline(device_, graphicsPipeline_, nullptr);
	vkDestroyPipelineLayout(device_, pipelineLayout_, nullptr);
	vkDestroyRenderPass(device_, renderPass_, nullptr);
	for (VkImageView& iv : swapchainImageViews_)
		vkDestroyImageView(device_, iv, nullptr);

	//update screen size
	vkFreeMemory(device_, vertexBufferMemory_, nullptr);
	vkDestroyBuffer(device_, vertexBuffer_, nullptr);

	//TODO does this work?
	//vertices_.erase();
	vertices_ =
	{
		{ { 0.0f, height_, 0.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { width_, height_, 0.0f },{ 0.0f, 1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ { width_, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 0.0f }, },
		{ { 0.0f, 0.0f, 0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0f, 0.0f } }
	};

	createVertexBuffer();

	//recreate
	createSwapchain();
	createImageViews();
	createRenderPass();
	createGraphicsPipeline();//TODO? can avoid this using dynamic state for scissor & viewport
	createFramebuffers();
	createCommandBuffers();
}

void ArchApp::createTextureImage()
{
	for (int i = 0; i < TEX_COUNT; ++i)
	{
		//load image file
		int texWidth, texHeight, texChannels;
		stbi_uc* pixels = stbi_load(textures_.path[i], &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
		VkDeviceSize imageSize = texWidth * texHeight * 4;
		if (!pixels) throw std::runtime_error("failed to load texture image!");

		//create vk image
		VkImage stagingImage;
		VkDeviceMemory stagingImageMemory;
		createImage(texWidth, texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_LINEAR, VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingImage, stagingImageMemory);

		//query byte arrangement (could have padding) and copy bytes
		VkImageSubresource subresource = {};
		subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		subresource.mipLevel = 0;
		subresource.arrayLayer = 0;
		VkSubresourceLayout stagingImageLayout;
		vkGetImageSubresourceLayout(device_, stagingImage, &subresource, &stagingImageLayout);
		void* data;
		vkMapMemory(device_, stagingImageMemory, 0, imageSize, 0, &data);
		if (stagingImageLayout.rowPitch == texWidth * 4)//no pad (image is likely pow(2) sized)
		{
			memcpy(data, pixels, (size_t)imageSize);
		}
		else
		{
			//account for padding
			uint8_t* dataBytes = reinterpret_cast<uint8_t*>(data);
			for (int y = 0; y < texHeight; y++)
				memcpy(&dataBytes[y * stagingImageLayout.rowPitch], &pixels[y * texWidth * 4], texWidth * 4);
		}
		vkUnmapMemory(device_, stagingImageMemory);
		stbi_image_free(pixels);

		createImage(texWidth, texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
			VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, textures_.image[i], textures_.memory[i]);

		//copy staged data into image
		transitionImageLayout(stagingImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
		transitionImageLayout(textures_.image[i], VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_PREINITIALIZED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		copyImage(stagingImage, textures_.image[i], texWidth, texHeight);
		transitionImageLayout(textures_.image[i], VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		vkFreeMemory(device_, stagingImageMemory, nullptr);
		vkDestroyImage(device_, stagingImage, nullptr);
	}

}

void ArchApp::createTextureImageView()
{
	for (int i = 0; i < TEX_COUNT; ++i)
		createImageView(textures_.image[i], VK_FORMAT_R8G8B8A8_UNORM, textures_.imageView[i]);
}

void ArchApp::createTextureSampler()
{
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_FALSE;//TODO performance?
	samplerInfo.maxAnisotropy = 1;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;

	if (vkCreateSampler(device_, &samplerInfo, nullptr, &textureSampler_) != VK_SUCCESS)
		throw std::runtime_error("failed to create texture sampler");
}

void ArchApp::createDescriptorSetLayout()
{
	std::array<VkDescriptorSetLayoutBinding, UNIFORM_COUNT + TEX_COUNT> bindings;
	//binding, descriptorType, descriptorCount, stageFlags, pImmutableSamplers
	bindings[0] = { 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT, nullptr };
	bindings[1] = { 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT, nullptr };
	for (int i = UNIFORM_COUNT; i < UNIFORM_COUNT + TEX_COUNT; ++i)
		bindings[i] = { uint32_t(i), VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_FRAGMENT_BIT, nullptr };

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	if (vkCreateDescriptorSetLayout(device_, &layoutInfo, nullptr, &descriptorSetLayout_) != VK_SUCCESS)
		throw std::runtime_error("failed to create descriptor set layout");
}

void ArchApp::createVertexBuffer()
{
	VkDeviceSize bufferSize = sizeof(vertices_[0]) * vertices_.size();

	//use staging buffer - use host visible buff only temporaryly to transfer data from cpu to gpu mem
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(device_, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices_.data(), (size_t)bufferSize);
	vkUnmapMemory(device_, stagingBufferMemory);

	//create actual vert bfr
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		vertexBuffer_, vertexBufferMemory_);

	copyBuffer(stagingBuffer, vertexBuffer_, bufferSize);

	//clear staging bfr
	vkFreeMemory(device_, stagingBufferMemory, nullptr);
	vkDestroyBuffer(device_, stagingBuffer, nullptr);
}

void ArchApp::createIndexBuffer()
{
	VkDeviceSize bufferSize = sizeof(indices_[0]) * indices_.size();

	//use staging buffer - use host visible buff only temporaryly to transfer data from cpu to gpu mem
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(device_, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices_.data(), (size_t)bufferSize);
	vkUnmapMemory(device_, stagingBufferMemory);

	//create actual vert bfr
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		indexBuffer_, indexBufferMemory_);

	copyBuffer(stagingBuffer, indexBuffer_, bufferSize);

	//clear staging bfr
	vkFreeMemory(device_, stagingBufferMemory, nullptr);
	vkDestroyBuffer(device_, stagingBuffer, nullptr);
}

void ArchApp::createGraphicsPipeline()
{
	//PROGRAMMABLE STAGES

	//vertex shader
	VkShaderModule vertShaderModule;
	std::vector<char> vertShaderCode = readFile("../shaders/quad.vert.spv");
	createShaderModule(vertShaderCode, vertShaderModule);

	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = vertShaderModule;
	vertShaderStageInfo.pName = "main";

	//fragment shader
	VkShaderModule fragShaderModule;
	std::vector<char> fragShaderCode = readFile("../shaders/mainScene.frag.spv");
	createShaderModule(fragShaderCode, fragShaderModule);

	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = fragShaderModule;
	fragShaderStageInfo.pName = "main";

	//all stage infos together for later
	VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

	//FIXED FUNCTION

	//vertex input
	VkVertexInputBindingDescription bindingDescription = Vertex::getBindingDescription();
	std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = Vertex::getAttributeDescriptions();

	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
	vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	//input assembly
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	//viewports
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapchainExtent_.width;
	viewport.height = (float)swapchainExtent_.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;
	//scissors
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapchainExtent_;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	//rasteriser
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f; // Optional
	rasterizer.depthBiasClamp = 0.0f; // Optional
	rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

											//multisampling
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f; // Optional
	multisampling.pSampleMask = nullptr; // Optional
	multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
	multisampling.alphaToOneEnable = VK_FALSE; // Optional

											   //TODO VkPipelineDepthStencilStateCreateInfo

											   //color blending
											   //1 VkPipelineColorBlendAttachmentState for each frame buffer
	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional
														 //global blend settings
	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f; // Optional
	colorBlending.blendConstants[1] = 0.0f; // Optional
	colorBlending.blendConstants[2] = 0.0f; // Optional
	colorBlending.blendConstants[3] = 0.0f; // Optional

											//TODO VkPipelineDynamicStateCreateInfo

											//PIPELINE LAYOUT
	VkDescriptorSetLayout setLayouts[] = { descriptorSetLayout_ };
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = setLayouts;
	pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
	pipelineLayoutInfo.pPushConstantRanges = 0; // Optional

	if (vkCreatePipelineLayout(device_, &pipelineLayoutInfo, nullptr, &pipelineLayout_) != VK_SUCCESS)
		throw std::runtime_error("failed to create pipeline layout");

	//GRAPHICS PIPELINE
	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	//shaders
	pipelineInfo.pStages = shaderStages;
	//fixed function
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = nullptr; // Optional
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = nullptr; // Optional
										  //pipeline layout
	pipelineInfo.layout = pipelineLayout_;
	//render pass
	pipelineInfo.renderPass = renderPass_;
	pipelineInfo.subpass = 0;
	//base pipeline to derive from
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1; // Optional

	if (vkCreateGraphicsPipelines(device_, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline_) != VK_SUCCESS)
		throw std::runtime_error("failed to create graphics pipeline");

	//cleanup shader modules
	vkDestroyShaderModule(device_, vertShaderModule, nullptr);
	vkDestroyShaderModule(device_, fragShaderModule, nullptr);
}
