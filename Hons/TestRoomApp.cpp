#include "TestRoomApp.h"

TestRoomApp::TestRoomApp()
	: BaseApplication()
	, frameTime_(0)
	, toggle_(true)
	, toggleCheck_(true)
	, toggle2_(true)
	, toggleCheck2_(true)
{
	camera_.setPosition(glm::vec4(470.0f, 470.0f, 450.0f, 1.0f));
	camera_.setRotation(glm::vec4(-45.0f, 45.0f, 0.0f, 0.0f));
}

TestRoomApp::~TestRoomApp()
{
	destroyVulkan();
}

void TestRoomApp::handleInput()
{
	int state;

	//camera movement
	state = glfwGetKey(window_, GLFW_KEY_E);
	if (state == GLFW_PRESS) camera_.moveUpward();

	state = glfwGetKey(window_, GLFW_KEY_Q);
	if (state == GLFW_PRESS) camera_.moveDownward();

	state = glfwGetKey(window_, GLFW_KEY_W);
	if (state == GLFW_PRESS) camera_.moveForward();

	state = glfwGetKey(window_, GLFW_KEY_A);
	if (state == GLFW_PRESS) camera_.strafeLeft();

	state = glfwGetKey(window_, GLFW_KEY_S);
	if (state == GLFW_PRESS) camera_.moveBackward();

	state = glfwGetKey(window_, GLFW_KEY_D);
	if (state == GLFW_PRESS) camera_.strafeRight();

	state = glfwGetKey(window_, GLFW_KEY_UP);
	if (state == GLFW_PRESS) camera_.turnUp();

	state = glfwGetKey(window_, GLFW_KEY_DOWN);
	if (state == GLFW_PRESS) camera_.turnDown();

	state = glfwGetKey(window_, GLFW_KEY_LEFT);
	if (state == GLFW_PRESS) camera_.turnLeft();

	state = glfwGetKey(window_, GLFW_KEY_RIGHT);
	if (state == GLFW_PRESS) camera_.turnRight();

	//toggle effects
	state = glfwGetKey(window_, GLFW_KEY_Z);
	if (state == GLFW_PRESS && toggleCheck_)
	{
		toggle_ = !toggle_;
		toggleCheck_ = false;
	}
	else if (state == GLFW_RELEASE) toggleCheck_ = true;

	state = glfwGetKey(window_, GLFW_KEY_X);
	if (state == GLFW_PRESS && toggleCheck2_)
	{
		toggle2_ = !toggle2_;
		toggleCheck2_ = false;
	}
	else if (state == GLFW_RELEASE) toggleCheck2_ = true;

	//quit
	state = glfwGetKey(window_, GLFW_KEY_ESCAPE);
	if (state == GLFW_PRESS) glfwSetWindowShouldClose(window_, true);
}

void TestRoomApp::initWindow()
{
	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);//don't init OpenGL

	window_ = glfwCreateWindow(width_, height_, "Test room scene", nullptr, nullptr);

	glfwSetWindowUserPointer(window_, this);
	glfwSetWindowSizeCallback(window_, onWindowResized);
}

void TestRoomApp::initVulkan()
{
	createInstance();
	setupDebugCallback();
	createSurface();
	pickPhysicalDevice();
	createLogicalDevice();
	createSwapchain();
	createImageViews();
	createRenderPass();
	createDescriptorSetLayout();
	createGraphicsPipeline();
	createFramebuffers();
	createCommandPools();
	createVertexBuffer();
	createIndexBuffer();
	createUniformBuffers();
	createDescriptorPool();
	createDescriptorSet();
	createCommandBuffers();
	createSemaphores();
}

void TestRoomApp::drawFrame()
{
	uint32_t imageIndex;
	VkResult result = vkAcquireNextImageKHR(device_, swapchain_, std::numeric_limits<uint64_t>::max(),
		imageAvailableSemaphore_, VK_NULL_HANDLE, &imageIndex);
	//if swapchain out of date, recreate it and return
	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		recreateSwapchain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
		throw std::runtime_error("failed to acquire swap chain image");

	VkSemaphore waitSemaphores[] = { imageAvailableSemaphore_ };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	VkSemaphore signalSemaphores[] = { renderFinishedSemaphore_ };
	VkSwapchainKHR swapchains[] = { swapchain_ };

	//submit info
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers_[imageIndex];
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	if (vkQueueSubmit(graphicsQueue_, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
		throw std::runtime_error("failed to submit draw command buffer");

	//present info
	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapchains;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr; // Optional

	result = vkQueuePresentKHR(presentQueue_, &presentInfo);
	//if swapchain out of date, recreate it
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
		recreateSwapchain();
	else if (result != VK_SUCCESS)
		throw std::runtime_error("failed to present swap chain image");
}

void TestRoomApp::mainLoop()
{
	//INIT_TIME
	while (!glfwWindowShouldClose(window_))
	{
		//START_TIME
		auto startTime = std::chrono::high_resolution_clock::now();

		//update
		glfwPollEvents();

		handleInput();
	
		camera_.update(frameTime_);
		updateUniformBuffers();
		
		//render
		drawFrame();
				auto currentTime = std::chrono::high_resolution_clock::now();
		frameTime_ = std::chrono::duration<float>(currentTime - startTime).count();
		//STOP_TIME
		//PRINT_TIME
}

	//make sure all threads are done before quitting
	vkDeviceWaitIdle(device_);
}

void TestRoomApp::destroyVulkan()
{
	//destroy order matteres

	//synchronisation
	vkDestroySemaphore(device_, renderFinishedSemaphore_, nullptr);
	vkDestroySemaphore(device_, imageAvailableSemaphore_, nullptr);

	//buffers
	vkFreeMemory(device_, indexBufferMemory_, nullptr);
	vkDestroyBuffer(device_, indexBuffer_, nullptr);
	vkFreeMemory(device_, vertexBufferMemory_, nullptr);
	vkDestroyBuffer(device_, vertexBuffer_, nullptr);

	//uniform buffers
	vkFreeMemory(device_, mvpBufferMemory_, nullptr);
	vkDestroyBuffer(device_, mvpBuffer_, nullptr);
	vkFreeMemory(device_, mvpStagingBufferMemory_, nullptr);
	vkDestroyBuffer(device_, mvpStagingBuffer_, nullptr);

	vkFreeMemory(device_, cameraBufferMemory_, nullptr);
	vkDestroyBuffer(device_, cameraBuffer_, nullptr);
	vkFreeMemory(device_, cameraStagingBufferMemory_, nullptr);
	vkDestroyBuffer(device_, cameraStagingBuffer_, nullptr);

	vkDestroyDescriptorPool(device_, descriptorPool_, nullptr);
	//graphics pipeline
	vkDestroyCommandPool(device_, commandPool_, nullptr);
	for (VkFramebuffer& fb : swapchainFramebuffers_)
		vkDestroyFramebuffer(device_, fb, nullptr);
	vkDestroyPipeline(device_, graphicsPipeline_, nullptr);
	vkDestroyPipelineLayout(device_, pipelineLayout_, nullptr);
	vkDestroyRenderPass(device_, renderPass_, nullptr);
	vkDestroyDescriptorSetLayout(device_, descriptorSetLayout_, nullptr);
	for (VkImageView& iv : swapchainImageViews_)
		vkDestroyImageView(device_, iv, nullptr);
	vkDestroySwapchainKHR(device_, swapchain_, nullptr);

	//
	vkDestroySurfaceKHR(instance_, surface_, nullptr);
	vkDestroyDevice(device_, nullptr);

	//debug callback
	if (enableValidationLayers)
	{
		//load function ptr and use it
		PFN_vkDestroyDebugReportCallbackEXT func =
			(PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance_, "vkDestroyDebugReportCallbackEXT");
		func(instance_, callback_, nullptr);
	}

	//
	vkDestroyInstance(instance_, nullptr);
}

void TestRoomApp::run()
{
	initWindow();
	initVulkan();
	mainLoop();
}

void TestRoomApp::createCommandPools()
{
	QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice_);

	//graphics
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;
	poolInfo.flags = 0; // Optional

	if (vkCreateCommandPool(device_, &poolInfo, nullptr, &commandPool_) != VK_SUCCESS)
		throw std::runtime_error("failed to create command pool");
}

void TestRoomApp::createCommandBuffers()
{
	//no need to clear command buffers at end of program since they are automatically destroyed whith their command pool,
	//however when recreating the swapchain we do not recreate cmd pool, so we need to check and destroy in that case
	if (commandBuffers_.size() > 0)
		vkFreeCommandBuffers(device_, commandPool_, commandBuffers_.size(), commandBuffers_.data());

	//need 1 cmd buff for each framebuffer
	commandBuffers_.resize(swapchainFramebuffers_.size());
	VkCommandBufferAllocateInfo allocInfo = {};

	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool_;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)commandBuffers_.size();

	if (vkAllocateCommandBuffers(device_, &allocInfo, commandBuffers_.data()) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate command buffers");

	for (size_t i = 0; i < commandBuffers_.size(); i++)
	{
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
		beginInfo.pInheritanceInfo = nullptr; // Optional

		vkBeginCommandBuffer(commandBuffers_[i], &beginInfo);

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = renderPass_;
		renderPassInfo.framebuffer = swapchainFramebuffers_[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = swapchainExtent_;
		VkClearValue clearColor = { 0.2f, 0.2f, 0.2f, 1.0f };
		renderPassInfo.clearValueCount = 1;
		renderPassInfo.pClearValues = &clearColor;

		//record commands
		vkCmdBeginRenderPass(commandBuffers_[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
		vkCmdBindPipeline(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline_);

		//bind buffers
		VkBuffer vertexBuffers[] = { vertexBuffer_ };
		VkDeviceSize offsets[] = { 0 };
		vkCmdBindVertexBuffers(commandBuffers_[i], 0, 1, vertexBuffers, offsets);
		vkCmdBindIndexBuffer(commandBuffers_[i], indexBuffer_, 0, VK_INDEX_TYPE_UINT16);

		//uniforms
		vkCmdBindDescriptorSets(commandBuffers_[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout_, 0, 1, &descriptorSet_, 0, nullptr);

		vkCmdDrawIndexed(commandBuffers_[i], indices_.size(), 1, 0, 0, 0);
		vkCmdEndRenderPass(commandBuffers_[i]);
		if (vkEndCommandBuffer(commandBuffers_[i]) != VK_SUCCESS)
			throw std::runtime_error("failed to record command buffer");
	}
}

void TestRoomApp::createUniformBuffers()
{
	//mvp
	VkDeviceSize bufferSize = sizeof(mvpUniform);

	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		mvpStagingBuffer_, mvpStagingBufferMemory_);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, mvpBuffer_, mvpBufferMemory_);

	//initialise here as it won't change
	mvpUniform mvp = {};
	mvp.model = glm::mat4();
	mvp.view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	mvp.proj = glm::ortho(0.0f, (float)width_, (float)height_, 0.0f, .0f, 5.0f);
	void* mvpdata;
	vkMapMemory(device_, mvpStagingBufferMemory_, 0, sizeof(mvp), 0, &mvpdata);
	memcpy(mvpdata, &mvp, sizeof(mvp));
	vkUnmapMemory(device_, mvpStagingBufferMemory_);
	copyBuffer(mvpStagingBuffer_, mvpBuffer_, sizeof(mvp));

	//camera
	bufferSize = sizeof(cameraUniform);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		cameraStagingBuffer_, cameraStagingBufferMemory_);
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, cameraBuffer_, cameraBufferMemory_);
}

void TestRoomApp::updateUniformBuffers()
{
	//camera
	cameraUniform cam = {};
	cam.position = camera_.position();
	cam.up = camera_.up();
	cam.forward = camera_.forward();
	cam.right = camera_.right();
	//TODO rename camera uniform to sceneData
	cam.toggles.x = int(toggle_);
	cam.toggles.y = int(toggle2_);
	cam.screenSize.x = width_;
	cam.screenSize.y = width_;

	//TODO should use push constants for frequently changing values?
	void* data;
	vkMapMemory(device_, cameraStagingBufferMemory_, 0, sizeof(cam), 0, &data);
	memcpy(data, &cam, sizeof(cam));
	vkUnmapMemory(device_, cameraStagingBufferMemory_);
	copyBuffer(cameraStagingBuffer_, cameraBuffer_, sizeof(cam));
}

void TestRoomApp::createDescriptorPool()
{
	std::array<VkDescriptorPoolSize, 1> poolSizes = {};
	//graphics: x2 uniforms, x2 combined tex sampler
	poolSizes[0] = { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, UNIFORM_COUNT };

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.maxSets = 1;
	poolInfo.poolSizeCount = poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();

	if (vkCreateDescriptorPool(device_, &poolInfo, nullptr, &descriptorPool_) != VK_SUCCESS)
		throw std::runtime_error("failed to create descriptor pool");
}

void TestRoomApp::createDescriptorSet()
{
	VkDescriptorSetLayout layouts[] = { descriptorSetLayout_ };
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool_;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = layouts;

	if (vkAllocateDescriptorSets(device_, &allocInfo, &descriptorSet_) != VK_SUCCESS)
		throw std::runtime_error("failed to allocate descriptor set");

	std::array<VkWriteDescriptorSet, UNIFORM_COUNT> descriptorWrites = {};
	VkDescriptorBufferInfo bufferInfo[2];

	//mvp matrix buffer
	bufferInfo[0].buffer = mvpBuffer_;
	bufferInfo[0].offset = 0;
	bufferInfo[0].range = sizeof(mvpUniform);

	descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[0].dstSet = descriptorSet_;
	descriptorWrites[0].dstBinding = 0;
	descriptorWrites[0].dstArrayElement = 0;
	descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[0].descriptorCount = 1;
	descriptorWrites[0].pBufferInfo = &bufferInfo[0];
	descriptorWrites[0].pImageInfo = nullptr; // Optional
	descriptorWrites[0].pTexelBufferView = nullptr; // Optional

													//camera buffer
	bufferInfo[1].buffer = cameraBuffer_;
	bufferInfo[1].offset = 0;
	bufferInfo[1].range = sizeof(cameraUniform);

	descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	descriptorWrites[1].dstSet = descriptorSet_;
	descriptorWrites[1].dstBinding = 1;
	descriptorWrites[1].dstArrayElement = 0;
	descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrites[1].descriptorCount = 1;
	descriptorWrites[1].pBufferInfo = &bufferInfo[1];
	descriptorWrites[1].pImageInfo = nullptr; // Optional
	descriptorWrites[1].pTexelBufferView = nullptr; // Optional

	vkUpdateDescriptorSets(device_, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
}

void TestRoomApp::recreateSwapchain()
{
	//wait for threads
	vkDeviceWaitIdle(device_);

	//destroy affected vk resources
	//note: createSwapchain() already handles old swapchain destruction internally
	for (VkFramebuffer& fb : swapchainFramebuffers_)
		vkDestroyFramebuffer(device_, fb, nullptr);
	vkDestroyPipeline(device_, graphicsPipeline_, nullptr);
	vkDestroyPipelineLayout(device_, pipelineLayout_, nullptr);
	vkDestroyRenderPass(device_, renderPass_, nullptr);
	for (VkImageView& iv : swapchainImageViews_)
		vkDestroyImageView(device_, iv, nullptr);

	//update screen size
	vkFreeMemory(device_, vertexBufferMemory_, nullptr);
	vkDestroyBuffer(device_, vertexBuffer_, nullptr);

	//TODO does this work?
	//vertices_.erase();
	vertices_ =
	{
		{ { 0.0f, height_, 0.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { width_, height_, 0.0f },{ 0.0f, 1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ { width_, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 0.0f }, },
		{ { 0.0f, 0.0f, 0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0f, 0.0f } }
	};

	createVertexBuffer();

	//recreate
	createSwapchain();
	createImageViews();
	createRenderPass();
	createGraphicsPipeline();//TODO? can avoid this using dynamic state for scissor & viewport
	createFramebuffers();
	createCommandBuffers();
}

void TestRoomApp::createDescriptorSetLayout()
{
	std::array<VkDescriptorSetLayoutBinding, UNIFORM_COUNT> bindings;
	//binding, descriptorType, descriptorCount, stageFlags, pImmutableSamplers
	bindings[0] = { 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT, nullptr };
	bindings[1] = { 1, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT, nullptr };

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	if (vkCreateDescriptorSetLayout(device_, &layoutInfo, nullptr, &descriptorSetLayout_) != VK_SUCCESS)
		throw std::runtime_error("failed to create descriptor set layout");
}

void TestRoomApp::createVertexBuffer()
{
	VkDeviceSize bufferSize = sizeof(vertices_[0]) * vertices_.size();

	//use staging buffer - use host visible buff only temporaryly to transfer data from cpu to gpu mem
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(device_, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices_.data(), (size_t)bufferSize);
	vkUnmapMemory(device_, stagingBufferMemory);

	//create actual vert bfr
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		vertexBuffer_, vertexBufferMemory_);

	copyBuffer(stagingBuffer, vertexBuffer_, bufferSize);

	//clear staging bfr
	vkFreeMemory(device_, stagingBufferMemory, nullptr);
	vkDestroyBuffer(device_, stagingBuffer, nullptr);
}

void TestRoomApp::createIndexBuffer()
{
	VkDeviceSize bufferSize = sizeof(indices_[0]) * indices_.size();

	//use staging buffer - use host visible buff only temporaryly to transfer data from cpu to gpu mem
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(device_, stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices_.data(), (size_t)bufferSize);
	vkUnmapMemory(device_, stagingBufferMemory);

	//create actual vert bfr
	createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		indexBuffer_, indexBufferMemory_);

	copyBuffer(stagingBuffer, indexBuffer_, bufferSize);

	//clear staging bfr
	vkFreeMemory(device_, stagingBufferMemory, nullptr);
	vkDestroyBuffer(device_, stagingBuffer, nullptr);
}

void TestRoomApp::createGraphicsPipeline()
{
	//PROGRAMMABLE STAGES

	//vertex shader
	VkShaderModule vertShaderModule;
	std::vector<char> vertShaderCode = readFile("../shaders/quad.vert.spv");
	createShaderModule(vertShaderCode, vertShaderModule);

	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = vertShaderModule;
	vertShaderStageInfo.pName = "main";

	//fragment shader
	VkShaderModule fragShaderModule;
	std::vector<char> fragShaderCode = readFile("../shaders/testRoomScene.frag.spv");
	createShaderModule(fragShaderCode, fragShaderModule);

	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = fragShaderModule;
	fragShaderStageInfo.pName = "main";

	//all stage infos together for later
	VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

	//FIXED FUNCTION

	//vertex input
	VkVertexInputBindingDescription bindingDescription = Vertex::getBindingDescription();
	std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = Vertex::getAttributeDescriptions();

	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
	vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
	vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	//input assembly
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	//viewports
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)swapchainExtent_.width;
	viewport.height = (float)swapchainExtent_.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;
	//scissors
	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = swapchainExtent_;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	//rasteriser
	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;
	rasterizer.depthBiasConstantFactor = 0.0f; // Optional
	rasterizer.depthBiasClamp = 0.0f; // Optional
	rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

											//multisampling
	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisampling.minSampleShading = 1.0f; // Optional
	multisampling.pSampleMask = nullptr; // Optional
	multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
	multisampling.alphaToOneEnable = VK_FALSE; // Optional

	//TODO VkPipelineDepthStencilStateCreateInfo
	//color blending
	//1 VkPipelineColorBlendAttachmentState for each frame buffer
	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional
														 //global blend settings
	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f; // Optional
	colorBlending.blendConstants[1] = 0.0f; // Optional
	colorBlending.blendConstants[2] = 0.0f; // Optional
	colorBlending.blendConstants[3] = 0.0f; // Optional

											//TODO VkPipelineDynamicStateCreateInfo

											//PIPELINE LAYOUT
	VkDescriptorSetLayout setLayouts[] = { descriptorSetLayout_ };
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = setLayouts;
	pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
	pipelineLayoutInfo.pPushConstantRanges = 0; // Optional

	if (vkCreatePipelineLayout(device_, &pipelineLayoutInfo, nullptr, &pipelineLayout_) != VK_SUCCESS)
		throw std::runtime_error("failed to create pipeline layout");

	//GRAPHICS PIPELINE
	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = 2;
	//shaders
	pipelineInfo.pStages = shaderStages;
	//fixed function
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = nullptr; // Optional
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = nullptr; // Optional
										  //pipeline layout
	pipelineInfo.layout = pipelineLayout_;
	//render pass
	pipelineInfo.renderPass = renderPass_;
	pipelineInfo.subpass = 0;
	//base pipeline to derive from
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1; // Optional

	if (vkCreateGraphicsPipelines(device_, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline_) != VK_SUCCESS)
		throw std::runtime_error("failed to create graphics pipeline");

	//cleanup shader modules
	vkDestroyShaderModule(device_, vertShaderModule, nullptr);
	vkDestroyShaderModule(device_, fragShaderModule, nullptr);
}
