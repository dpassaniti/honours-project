#include <iostream>
#include <stdexcept>
#define STB_IMAGE_IMPLEMENTATION // required fo stb img loading lib
#include "ArchApp.h"
#include "FractalApp.h"
#include "TestRoomApp.h"

int main()
{
	BaseApplication* app = nullptr;

	bool listen = true;
	bool running = true;
	int input = 0;

	//select scenario
	do
	{
		std::cout << "Type ID and enter: " << std::endl;
		std::cout << "1 - Architecture scene " << std::endl;
		std::cout << "2 - Fractal scene " << std::endl;
		std::cout << "3 - Test Room scene " << std::endl;
		std::cout << "4 - Quit " << std::endl;
		std::cin >> input;
		//if input is not an integer, avoid endless loop
		if (!std::cin)
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		switch (input)
		{
		case 1:
			app = new ArchApp;
			listen = false;
			break;
		case 2:
			app = new FractalApp;
			listen = false;
			break;
		case 3:
			app = new TestRoomApp;
			listen = false;
			break;
		case 4:
			delete app;
			app = nullptr;
			return EXIT_SUCCESS;
			break;
		default:
			break;
		}
	} while (listen);

	//run selected scene
	try
	{
		app->run();
	}
	catch (const std::runtime_error& e)
	{
		std::cerr << e.what() << std::endl;
		delete app;
		app = nullptr;
		int a;
		std::cin >> a;
		return EXIT_FAILURE;
	}

	delete app;
	app = nullptr;
	return EXIT_SUCCESS;
}
