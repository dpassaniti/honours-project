#ifndef _TEST_ROOM_APP_
#define _TEST_ROOM_APP_
#include "BaseApplication.h"
#include <chrono>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Camera.h"

class TestRoomApp : public BaseApplication
{

public:
	TestRoomApp();
	~TestRoomApp();

	void run();

private:
	static const int UNIFORM_COUNT = 2;

	std::vector<Vertex> vertices_ =
	{
		{ { 0.0f, sHEIGHT, 0.0f },{ 1.0f, 0.0f, 0.0f },{ 0.0f, 1.0f } },
		{ { sWIDTH, sHEIGHT, 0.0f },{ 0.0f, 1.0f, 0.0f },{ 1.0f, 1.0f } },
		{ { sWIDTH, 0.0f, 0.0f },{ 0.0f, 0.0f, 1.0f },{ 1.0f, 0.0f }, },
		{ { 0.0f, 0.0f, 0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0f, 0.0f } }
	};
	const std::vector<uint16_t> indices_ = { 0, 1, 2, 2, 3, 0 };

	//TODO mul these and send just 1
	struct mvpUniform
	{
		glm::mat4 model;
		glm::mat4 view;
		glm::mat4 proj;
	};

	struct cameraUniform
	{
		glm::vec4 position;
		glm::vec4 up;
		glm::vec4 forward;
		glm::vec4 right;

		glm::vec2 screenSize;
		glm::vec2 toggles;
	};
	bool toggle_;//toggle effects in shader
	bool toggleCheck_;
	bool toggle2_;//toggle effects in shader
	bool toggleCheck2_;

	//scenevars
	Camera camera_;
	float frameTime_;

	VkBuffer cameraBuffer_;
	VkDeviceMemory cameraBufferMemory_;
	VkBuffer cameraStagingBuffer_;
	VkDeviceMemory cameraStagingBufferMemory_;

	//core
	void initWindow();
	void initVulkan();
	void drawFrame();
	void mainLoop();
	void destroyVulkan();
	void handleInput();

	void recreateSwapchain();
	void createGraphicsPipeline();

	static void onWindowResized(GLFWwindow* window, int width, int height)
	{
		if (width == 0 || height == 0) return;//minimized

		TestRoomApp* app = reinterpret_cast<TestRoomApp*>(glfwGetWindowUserPointer(window));
		app->recreateSwapchain();
	};

	//uniforms, buffers and command queues
	void createVertexBuffer();
	void createIndexBuffer();
	void createUniformBuffers();
	void updateUniformBuffers();
	void createDescriptorSetLayout();
	void createDescriptorSet();
	void createCommandPools();
	void createCommandBuffers();
	void createDescriptorPool();
};

#endif

