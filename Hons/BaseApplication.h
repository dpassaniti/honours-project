/*
* vulkan framework based on Alexander Overvoorde's tutorial at vulkan-tutorial.com
*/

#ifndef _BASE_APPLICATION_H_
#define _BASE_APPLICATION_H_

#define INIT_TIME \
	int count = 0;\
	double avg = 0;
#define START_TIME \
	auto t1 = std::chrono::high_resolution_clock::now();
#define STOP_TIME \
	auto t2 = std::chrono::high_resolution_clock::now();\
	avg += std::chrono::duration<double,std::milli>(t2 - t1).count();
#define PRINT_TIME \
	if(++count == 50)\
	{\
		std::cout << avg / double(count) << ",";\
		avg = 0;\
		count = 0;\
	}

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <stdexcept>
#include <vector>
#include <array>
#include <set>
#include <algorithm>
#include <fstream>
#include <iostream>
#include "Vertex.h"

#include <stb_image.h>//load png,jpg,...

class BaseApplication
{
#define sWIDTH 1280.0f
#define sHEIGHT 720.0f

public:
	BaseApplication();
	~BaseApplication();

	virtual void run() = 0;

protected:
	const bool enableValidationLayers;
	const std::vector<const char*> validationLayers;
	const std::vector<const char*> deviceExtensions;
	const uint32_t width_;
	const uint32_t height_;
	GLFWwindow* window_;

	//vulkan members
	VkInstance instance_;
	VkDebugReportCallbackEXT callback_;
	VkSurfaceKHR surface_;
	VkPhysicalDevice physicalDevice_;
	VkDevice device_;
	VkQueue graphicsQueue_;
	VkQueue presentQueue_;
	VkSwapchainKHR swapchain_;
	std::vector<VkImage> swapchainImages_;
	VkFormat swapchainImageFormat_;
	VkExtent2D swapchainExtent_;
	std::vector<VkImageView> swapchainImageViews_;
	VkRenderPass renderPass_;
	VkDescriptorSetLayout descriptorSetLayout_;
	VkPipelineLayout pipelineLayout_;
	VkPipeline graphicsPipeline_;
	std::vector<VkFramebuffer> swapchainFramebuffers_;
	VkCommandPool commandPool_;
	std::vector<VkCommandBuffer> commandBuffers_;
	VkSemaphore imageAvailableSemaphore_;
	VkSemaphore renderFinishedSemaphore_;
	VkDescriptorPool descriptorPool_;//both pipelines
	VkDescriptorSet descriptorSet_;

	//uniforms and buffers for quad canvas
	VkBuffer vertexBuffer_;
	VkDeviceMemory vertexBufferMemory_;
	VkBuffer indexBuffer_;
	VkDeviceMemory indexBufferMemory_;

	VkBuffer mvpBuffer_;
	VkDeviceMemory mvpBufferMemory_;
	VkBuffer mvpStagingBuffer_;
	VkDeviceMemory mvpStagingBufferMemory_;

	//getters
	int getWidth() { return width_; };
	int getHeigh() { return height_; };

	//Vk calls this function when something goes wrong (if layers enabled)
	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback
	(VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layerPrefix,
		const char* msg,
		void* userData)
	{
		std::cerr << "validation layer: " << msg << std::endl;
		return VK_FALSE;
	}

	//read shader source
	static std::vector<char> readFile(const std::string& filename);

	struct QueueFamilyIndices
	{
		int graphicsFamily = -1;
		int presentFamily = -1;
		int computeFamily = -1;
		bool isComplete()
		{
			return graphicsFamily >= 0 && presentFamily >= 0 && computeFamily >= 0;
		}
	};

	struct SwapchainSupportDetails
	{
		VkSurfaceCapabilitiesKHR capabilities;
		std::vector<VkSurfaceFormatKHR> formats;
		std::vector<VkPresentModeKHR> presentModes;
	};

	//pure virtuals
	virtual void recreateSwapchain() = 0;
	virtual void initWindow() = 0;
	virtual void initVulkan() = 0;
	virtual void drawFrame() = 0;
	virtual void mainLoop() = 0;
	virtual void destroyVulkan() = 0;
	virtual void handleInput() = 0;
	
	//utils
	SwapchainSupportDetails querySwapchainSupport(VkPhysicalDevice device);
	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);
	//[in] ptr to vector to be populated with extension names
	void getRequiredExtensions(std::vector<const char*>* extensions);
	bool checkValidationLayerSupport();
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	bool isDeviceSuitable(VkPhysicalDevice device);
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
	void createShaderModule(const std::vector<char>& code, VkShaderModule& shaderModule);
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
		       	VkBuffer& buffer, VkDeviceMemory& bufferMemory);
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
	VkCommandBuffer beginSingleTimeCommands();//for graphics pipeline
	void endSingleTimeCommands(VkCommandBuffer commandBuffer);
	void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
	void createImageView(VkImage image, VkFormat format, VkImageView &imageView);

	void createInstance();
	void setupDebugCallback();
	void createSurface();
	void pickPhysicalDevice();
	void createLogicalDevice();
	void createSwapchain();
	void createImageViews();//swapchain's image views
	void createRenderPass();
	void createGraphicsPipeline();
	void createFramebuffers();
	void createSemaphores();

	void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
		VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);
	void copyImage(VkImage srcImage, VkImage dstImage, uint32_t width, uint32_t height);
};

#endif//_BASE_APPLICATION_H_

