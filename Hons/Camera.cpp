#include "camera.h"

Camera::Camera()
{
	position_ = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	rotation_ = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	lookAt_ = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	frameTime_ = 0.1f;

	speed_ = 30.0f;
	rotationSpeed_ = 150.0f;
}

Camera::~Camera()
{
}

void Camera::update(float frameTime)
{
	float yaw, pitch, roll;
	glm::mat4 rotationMatrix;
	
	frameTime_ = frameTime;

	// Setup the vector that points upwards.
	up_ = glm::vec4(0.0f, -1.0, 0.0, 0.0f);
	
	// Setup where the camera is looking by default.
	lookAt_ = glm::vec4(0.0, 0.0, -1.0f, 1.0f);
	
	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	// pi/180 = 0.0174532925  
	roll = rotation_.x * 0.0174532925f;
	yaw = rotation_.y * 0.0174532925f;
	pitch = rotation_.z * 0.0174532925f;
	
	// Create the rotation quaternion from the yaw, pitch, and roll values.
	glm::vec3 eulers(roll, yaw, pitch);
	glm::quat rotation(eulers);

	// Transform the lookAt and up vector by the rotation quaterniion so the view is correctly rotated at the origin.
	lookAt_ = rotation * lookAt_;
	up_ =  glm::normalize(rotation * up_);

	// Translate the rotated camera position to the location of the viewer.
	lookAt_ = position_ + lookAt_;

	// calculate forward and right vectors
	forward_ = glm::normalize(lookAt_-position_);
	right_ = glm::normalize(glm::vec4(glm::cross(glm::vec3(up_), glm::vec3(forward_)), 0.0f));
}

void Camera::moveForward()
{
	float radians;

	// Convert degrees to radians.
	radians = rotation_.y * 0.0174532925f;

	// Update the position.
	position_.x -= glm::sin(radians) * frameTime_ * speed_;
	position_.z -= glm::cos(radians) * frameTime_ * speed_;
}

void Camera::moveBackward()
{
	float radians;

	// Convert degrees to radians.
	radians = rotation_.y * 0.0174532925f;

	// Update the position.
	position_.x += glm::sin(radians) * frameTime_ * speed_;
	position_.z += glm::cos(radians) * frameTime_ * speed_;
}

void Camera::moveUpward()
{
	// Update the height position.
	position_.y += frameTime_ * speed_;
}

void Camera::moveDownward()
{
	// Update the height position.
	position_.y -= frameTime_ * speed_;
}

void Camera::turnRight()
{
	// Update the rotation.
	rotation_.y -= frameTime_ * rotationSpeed_;
}

void Camera::turnLeft()
{
	// Update the rotation.
	rotation_.y += frameTime_ * rotationSpeed_;
}

void Camera::turnDown()
{

	// Update the rotation.
	rotation_.x -= frameTime_ * rotationSpeed_;
}

void Camera::turnUp()
{
	// Update the rotation.
	rotation_.x += frameTime_ * rotationSpeed_;
}

void Camera::strafeRight()
{
	// Convert degrees to radians.
	float radians = rotation_.y * 0.0174532925f;

	// Update the position.
	position_.z -= glm::sin(radians) * frameTime_ * speed_;
	position_.x += glm::cos(radians) * frameTime_ * speed_;
}

void Camera::strafeLeft()
{
	// Convert degrees to radians.
	float radians = rotation_.y * 0.0174532925f;

	// Update the position.
	position_.z += glm::sin(radians) * frameTime_ * speed_;
	position_.x -= glm::cos(radians) * frameTime_ * speed_;
}
