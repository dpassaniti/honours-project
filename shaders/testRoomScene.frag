#version 450
#extension GL_ARB_separate_shader_objects : enable

#define ROOM_SIZE 500
vec3 lightPos = vec3(230, 230, 230);
layout(binding = 1) uniform cameraData
	{
		vec4 position;
		vec4 up;
		vec4 forward;
		vec4 right;

		vec2 screenSize;
		vec2 toggles;//TODO specialisation vars
	} camera;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

//UTILS

//twist input position on xz based on y
//breaks lipschitz
//strength 0.3 -> march step/4 for proper rendering
vec3 twistPos(vec3 pos, float strength)
{
    float c = cos(strength*pos.y);
    float s = sin(strength*pos.y);
    mat2  rotmat = mat2(c,-s,s,c);
    return vec3(rotmat*pos.xz,pos.y);
}

//DISTANCE FIELDS

float box(vec3 position, vec3 size)
{
	vec3 d = abs(position) - size;
	return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

float sphere(vec3 position, float radius)
{
	return length(position) - radius;
}

//SCENE
float scene(vec3 pos, inout int material)
{
	//change colour with x position
	material = int(pos.x/100);

	//walls of the room
	float walls = -box(pos, vec3(ROOM_SIZE));

	//spheres
	float period = 65;
	vec3 modpos = mod(pos + vec3(period/2), period) - vec3(period/2);	
//	float ball = sphere(modpos, 10.0);
	float ball = sphere(modpos, 10.0);//max(-sphere(modpos, 9.0),box(modpos,vec3(7)));

	//select number of spheres
	//room size still limits max visible
	float nOfSpheresRoot3 = 7;//use odd number or borders are cut off
	float limitSize = period*nOfSpheresRoot3/2;
	float limit = box(pos, vec3(limitSize));

	return min(max(ball,limit), walls);
}

vec4 getMaterial(int material, inout float reflectiveness)
{
	reflectiveness = 0.6;

	//color based on unit digit
	switch(int(mod(material, 5)))
	{
	case 0:
		return vec4(0.95,0.11,0.15,1.0);
		break;
	case 1:
		return vec4(0.04,0.14,0.39,1.0);
		break;
	case 2:
		return vec4(0.98,0.85,0.20,1.0);
		break;
	case 3:
		return vec4(0.59,0.99,0.48,1.0);
		break;
	case 4:
		return vec4(0.44,0.78,0.75,1.0);
		break;
	case 5:
		return vec4(0.90,0.15,0.49, 1.0);
		break;
	case 6:
		return vec4(0.67,0.59,0.46, 1.0);
		break;
	case 7:
		return vec4(0.58,0.39,0.64, 1.0);
		break;
	case 8:
		return vec4(0.51,0.45,0.45, 1.0);
		break;
	case 9:
		return vec4(0.95,0.77,0.85, 1.0);
		break;
	default:
		return vec4(0.0, 0.0, 0.0, 1.0);
		break;
	};
}

vec3 getNormalAt(vec3 pos)
{
	vec3 n;
	float dt = 0.01;
	int dummy;//don't need material info from normal samples

	vec3 samplePoint = vec3(pos.x + dt, pos.y, pos.z);
	vec3 samplePoint2 = vec3(pos.x - dt, pos.y, pos.z);
	n.x = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	samplePoint = vec3(pos.x, pos.y + dt, pos.z);
	samplePoint2 = vec3(pos.x, pos.y - dt, pos.z);
	n.y = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	samplePoint = vec3(pos.x, pos.y, pos.z + dt);
	samplePoint2 = vec3(pos.x, pos.y, pos.z - dt);
	n.z = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	return normalize(n);
}

float getShadow(vec3 pos, vec3 lightDir)
{
	int dummy;
	float shadow = 1.0;
	float maxdist = length(lightPos - pos);
	float dist = 0.1;
	float shadowHardness = 32;
	while(dist < maxdist)
	{
		float shadowPos = scene(pos + lightDir*dist, dummy);
		if( shadowPos < 0.01 )
		{
			shadow = 0.0;
			break;
		}
		shadow = min( shadow, shadowHardness*shadowPos/dist );
		dist += shadowPos;
	}

	return shadow;
}

vec4 getReflectColor(vec3 pos, int material, vec3 rayDirection, vec3 lightPos, vec4 lightColor)
{
	vec3 n = getNormalAt(pos);

	float dummy;//no recursive reflections
	vec4 materialColor = getMaterial(material, dummy);
	
	//lighting
	vec3 lightDir = normalize(lightPos - pos);
	//vec3 lightDir = vec3(1.0, 0.5, .25);//directional sunlight
	vec4 intensity = lightColor * dot(n, lightDir);
	
	float shadow = getShadow(pos, lightDir);

	return materialColor*intensity*shadow;
}

vec4 shadeScene(vec3 pos, int material, vec3 rayDirection, vec4 lightColor)
{
	vec3 n = getNormalAt(pos);

	//apply material
	float reflectiveness = 0;
	vec4 materialColor = getMaterial(material, reflectiveness);
	vec4 finalColor = materialColor;

	//lighting
//	vec3 lightDir = normalize(vec3(1.0, 0.5, .25));//directional sunlight
	vec3 lightDir = normalize(lightPos- pos);
	vec4 intensity = lightColor * dot(n, lightDir);
	finalColor *= intensity;
	
	//compute shadows
	if(camera.toggles.x == 1)
	{
		float shadow = getShadow(pos, lightDir);

		finalColor *= shadow;
	}
	//compute reflections
	if(camera.toggles.y == 1)
	{	
		//reflections
		vec4 reflectionColor = vec4(1.0); 
		if(reflectiveness > 0.0)
		{
			float dist = 0.1;
			vec3 direction = reflect(rayDirection, n);
			int rMat;
			while(dist < ROOM_SIZE*2)
			{
				float reflectPos = scene(pos + direction*dist, rMat);
				if(reflectPos < 0.01)
				{
					reflectionColor = getReflectColor(pos + direction*dist, rMat, direction, lightPos, lightColor);
					reflectionColor = mix(materialColor, reflectionColor, reflectiveness);
					break;
				}
				dist += reflectPos;
			}
		}
		finalColor *= reflectionColor;
	}

	return finalColor;
}

void main()
{
	//max loops
	int maxSteps = 512*10;
	
	//camera data
	vec3 eye = vec3(camera.position);
	vec3 right = vec3(camera.right);
	vec3 forward = vec3(camera.forward);
	vec3 up = vec3(camera.up);

	//shading
	vec4 lightColor = vec4(1.0,1.0,1.0, 1.0);
	int material;

	//map pixel coordinate to viewport
	float u = -1.0 + 2.0 * gl_FragCoord.x / camera.screenSize.x;
	float v = -1.0 + 2.0 * gl_FragCoord.y / camera.screenSize.y;
	//u *= camera.screenSize.x/camera.screenSize.y;

	//ray
	float f = 4.0;//focal length
	vec3 rayDirection = normalize(right * u + up * v + forward*f);

	//default colour (miss)
	vec4 color = vec4(0.3, 0.3, 0.3, 0.0);
	
	//distance travelled along the ray
	float raydist = 0.0;
	for(int i = 0; i < maxSteps; ++i)
	{
		//move along the ray
		vec3 position = eye + rayDirection * raydist;
		
		//check distance of position from surface
		float dist = scene(position, material);

		if(dist < 0.01)
		{
			//surface colour
			color = shadeScene(position, material, rayDirection, lightColor);
			break;
		}
//		else if(raydist > 700) break;//line of sight

		raydist += dist;
	}

	outColor = color;
}
