#version 450
#extension GL_ARB_separate_shader_objects : enable

#define MAT_COUNT 4

#define GROUND_MAT 0
#define TOP_MAT 1
#define COLUMN_MAT 2
#define SKYBOX_MAT 3

#define SKYBOX_RADIUS 600

layout(binding = 1) uniform cameraData
	{
		vec4 position;
		vec4 up;
		vec4 forward;
		vec4 right;

		vec2 screenSize;
		float time;
	} camera;

layout(binding = 2) uniform sampler2D groundMat;
layout(binding = 3) uniform sampler2D topMat;
layout(binding = 4) uniform sampler2D columnMat;
layout(binding = 5) uniform sampler2D skyboxMat;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

//UTILS

// polynomial smooth min (k = 0.1);
float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

//fold 2 axes along each other given range
vec2 fold2(vec2 axes, float range)
{
	vec2 newPos;

	float pos1 = abs(axes.x) - range;
	float pos2 = abs(axes.y) - range;

	if(pos1 < pos2)
	{
		newPos.x = pos1;
		newPos.y = pos2;
	}
	else
	{
		newPos.x = pos2;
		newPos.y = pos1;
	}

	return newPos;
}

//find closest surface
int pickMaterial(float dists[MAT_COUNT])
{
	int material = -1;
	float closest = 1;//at least one value is <1 when the return value is relevant in following usage

	for(int i = 0; i < MAT_COUNT; ++i)
		if(dists[i] < closest)
		{
			closest = dists[i];
			material = i;
		}

	return material;
}

//DISTANCE FIELDS

float box(vec3 position, vec3 size)
{
	vec3 d = abs(position) - size;
	return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

float endlessBox(vec2 position, vec2 size)
{
	vec2 d = abs(position) - size;
	return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

float sphere(vec3 position, float radius)
{
	return length(position) - radius;
}

float plane(vec3 position, vec4 normal)
{
	// n must be normalized
	return dot(position, normal.xyz) + normal.w;
}

float cylinderCapped(vec3 pos, vec2 h)
{
	vec2 d = abs(vec2(length(pos.xz),pos.y)) - h;
	return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

float cylinder(vec3 pos, vec2 centre, float size)
{
	return length(pos.xy - centre.xy) - size;
}

//SCENE
float scene(vec3 pos, inout int material)
{
	vec3 originalP = pos;

	//repeat space on xz
//	pos.xz = mod(pos.xz + vec2(100.0), 200.0) - vec2(100.0);
	//fold x axis on z axis
	pos.xz = fold2(pos.xz, 20);
	pos.xz = fold2(pos.xz, 20);
	//mirror along z
	float wallDistance = 15.0;
	pos.z = abs(pos.z) - wallDistance;

	//repeat interval value for building parts
	float interval = 10;

	//COLUMNS
	vec3 columnPos = pos;
	float columnHeight = 7.0;
	//repeat on x
	columnPos.x = mod(pos.x + interval*0.5, interval) - interval *0.5;
	//mirror to have 2
	columnPos.x = abs(columnPos.x) - 1;

	float columns = cylinderCapped(columnPos, vec2(.75, columnHeight));

	//TOP
	float topHeight = 5;
	//move to correct y position
	vec3 topPos = pos - vec3(0.0, columnHeight + topHeight, 0.0);
	//TOP arch
	vec3 archPos = pos;
	//repeat on x
	archPos.x = mod(pos.x , interval) - interval*0.5;
	//mirror on x for non circulare shape
	archPos.x = abs(archPos.x) + 1.5;
	float archRadius = 4.5;
	float arch = cylinder(archPos, vec2(0.0, columnHeight + columnHeight/4.0), archRadius);
	//TOP arch detail(fissure)
	//repeat and mirror same coords again
	archPos.x = mod(pos.x + interval*0.25, interval*0.5) - interval*0.25;
	archPos.x = abs(archPos.x) + 1.5;
	float fissure = cylinder(archPos, vec2(0.0, columnHeight + columnHeight/1.5), archRadius/2.0);
	//TOP top hole
	vec3 holePos = pos;
	holePos.x = mod(pos.x + interval*0.5, interval) - interval*0.5;
	holePos.y -= 2.0;
	float hole = cylinder(holePos, vec2(0.0, columnHeight + columnHeight/1.5), archRadius/3);
	
	float top = endlessBox(topPos.zy, vec2(1.0, topHeight));
	top = max(top, -fissure);
	top = max(top, -arch);
	top = max(top, -hole);

	//ROOF > same material as top
	float roofHeight = 0.5;
	//move to correct position
	vec3 roofPos = pos - vec3(0.0, columnHeight + (topHeight*2) - roofHeight, 0.0);

	float roof = endlessBox(roofPos.zy, vec2(wallDistance/6, roofHeight));

	//GROUND
	float ground = plane(originalP + vec3(0,columnHeight/2,0), normalize(vec4(0.0, 1.0, 0.0, 1.0)));
	//ground bumps
	vec3 bumpPos = originalP;
	float sphereBump = sphere(bumpPos + vec3(0, 12*sin(camera.time),0), 7);
	bumpPos.xz = fold2(bumpPos.xz, 110);
	float cubeBump = box(bumpPos + vec3(columnHeight), vec3(50,25,10));

	ground = smin(sphereBump, ground, 8);
	ground = min(cubeBump, ground);
	
	//BUILDING
	top = min(top, roof);
	float building = min(top, columns);

	//SKYBOX
	float skybox = -sphere(originalP, SKYBOX_RADIUS);

	float final = min(ground, building);
	final = min(final, skybox);

	//DETERMINE MATERIAL
	float distances[] = {ground, top, columns, skybox};
	material = pickMaterial(distances);

	return final;
}


vec4 getMaterial(int material, vec3 texTiling, vec3 normal, inout float reflectiveness)
{
	vec4 sample1, sample2, sample3;

	switch(material)
	{
	case GROUND_MAT:
		texTiling /= 20;
		reflectiveness = 0.6;
		sample1 = texture(groundMat, texTiling.yz) * abs(normal.x);
		sample2 = texture(groundMat, texTiling.xz) * abs(normal.y);
		sample3 = texture(groundMat, texTiling.xy) * abs(normal.z);
		break;
	case TOP_MAT:
		texTiling /= 10;
		sample1 = texture(topMat, texTiling.yz) * abs(normal.x);
		sample2 = texture(topMat, texTiling.xz) * abs(normal.y);
		sample3 = texture(topMat, texTiling.xy) * abs(normal.z);
		break;
	case COLUMN_MAT:
		texTiling /= 5;
		sample1 = texture(columnMat, texTiling.yz) * abs(normal.x);
		sample2 = texture(columnMat, texTiling.xz) * abs(normal.y);
		sample3 = texture(columnMat, texTiling.xy) * abs(normal.z);
		break;
	case SKYBOX_MAT:
		texTiling /= SKYBOX_RADIUS;
		sample1 = texture(skyboxMat, texTiling.yz) * abs(normal.x);
		sample2 = texture(skyboxMat, texTiling.xz) * abs(normal.y);
		sample3 = texture(skyboxMat, texTiling.xy) * abs(normal.z);
		break;
	default:
		sample1 = vec4(1.0, 0.0, 0.0, 1.0);
		sample2 = vec4(1.0, 0.0, 0.0, 1.0);
		sample3 = vec4(1.0, 0.0, 0.0, 1.0);
		break;
	};

	return (sample1 + sample2 + sample3);
}

vec3 getNormalAt(vec3 pos)
{
	vec3 n;
	float dt = 0.01;
	int dummy;//don't need material info from normal samples

	vec3 samplePoint = vec3(pos.x + dt, pos.y, pos.z);
	vec3 samplePoint2 = vec3(pos.x - dt, pos.y, pos.z);
	n.x = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	samplePoint = vec3(pos.x, pos.y + dt, pos.z);
	samplePoint2 = vec3(pos.x, pos.y - dt, pos.z);
	n.y = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	samplePoint = vec3(pos.x, pos.y, pos.z + dt);
	samplePoint2 = vec3(pos.x, pos.y, pos.z - dt);
	n.z = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	return normalize(n);
}

float getShadow(vec3 pos, vec3 lightPos, vec3 lightDir)
{
	int dummy;
	float shadow = 1.0;
	float maxdist = length(lightPos - pos);
	float dist = 0.1;
	float shadowHardness = 32;
	while(dist < maxdist)
	{
		float shadowPos = scene(pos + lightDir*dist, dummy);
		if( shadowPos < 0.01 )
		{
			shadow = 0.0;
			break;
		}
		shadow = min( shadow, shadowHardness*shadowPos/dist );
		dist += shadowPos;
	}

	return shadow;
}

vec4 getReflectColor(vec3 pos, int material, vec3 rayDirection, vec3 lightPos, vec4 lightColor)
{
	vec3 n = getNormalAt(pos);

	float dummy;//no recursive reflections
	vec4 materialColor = getMaterial(material, pos, n, dummy);
	
	//lighting
	vec3 lightDir = normalize(lightPos - pos);
	//vec3 lightDir = vec3(1.0, 0.5, .25);//directional sunlight
	vec4 intensity = lightColor * dot(n, lightDir);
	
	float shadow = getShadow(pos, lightPos, lightDir);

	return materialColor*intensity*shadow;
}

vec4 shadeScene(vec3 pos, int material, vec3 rayDirection, vec3 lightPos, vec4 lightColor)
{
	vec3 n = getNormalAt(pos);

	//apply material
	float reflectiveness = 0.0;
	vec4 materialColor = getMaterial(material, pos, n, reflectiveness);

	vec4 finalColor = materialColor;

	if(material != SKYBOX_MAT)//no lighting on skybox
	{
		//reflections
		vec4 reflectionColor = vec4(1.0); 
		if(reflectiveness > 0.0)
		{
			float dist = 0.1;
			vec3 direction = reflect(rayDirection, n);
			int rMat;
			while(dist < SKYBOX_RADIUS*2)
			{
				float reflectPos = scene(pos + direction*dist, rMat);
				if(reflectPos < 0.01)
				{
					reflectionColor = getReflectColor(pos + direction*dist, rMat, direction, lightPos, lightColor);
					reflectionColor = mix(materialColor, reflectionColor, reflectiveness);
					break;
				}
				dist += reflectPos;
			}
		}

		//lighting
		vec3 lightDir = normalize(lightPos - pos);
		//vec3 lightDir = vec3(1.0, 0.5, .25);//directional sunlight
		vec4 intensity = lightColor * dot(n, lightDir);
		
		float shadow = getShadow(pos, lightPos, lightDir);

		finalColor *= reflectionColor*intensity*shadow;
	}

	return finalColor;
}

void main()
{
	//max loops
	int maxSteps = 512;
	
	//camera data
	vec3 eye = vec3(camera.position);
	vec3 right = vec3(camera.right);
	vec3 forward = vec3(camera.forward);
	vec3 up = vec3(camera.up);

	//shading
	vec3 lightPos = vec3(40.0, 100.0, 70.0);
	lightPos += vec3(sin(camera.time/10)*50, sin(camera.time/5)*30, cos(camera.time/10)*50);
	vec4 lightColor = vec4(1.0,1.0,1.0, 1.0);
	int material;

	//map pixel coordinate to viewport
	float u = -1.0 + 2.0 * gl_FragCoord.x / camera.screenSize.x;
	float v = -1.0 + 2.0 * gl_FragCoord.y / camera.screenSize.y;
	//u *= camera.screenSize.x/camera.screenSize.y;

	//ray
	float f = 4.0;//focal length
	vec3 rayDirection = normalize(right * u + up * v + forward*f);

	//default colour (miss)
	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);//texture(skyMat, fragTexCoord);
	
	//distance travelled along the ray
	float raydist = 0.0;
	float lightd = 0.0;
	for(int i = 0; i < maxSteps; ++i)
	{
		//move along the ray
		vec3 position = eye + rayDirection * raydist;
		
		//check distance of position from surface
		float dist = scene(position, material);

		if(dist < 0.01)
		{
			//surface colour
			color = shadeScene(position, material, rayDirection, lightPos, lightColor);
			break;
		}
//		else if(raydist > 600) break;//line of sight

		raydist += dist;
	}

	outColor = color;
}
