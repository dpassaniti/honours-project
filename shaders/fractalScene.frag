#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform cameraData
	{
		vec4 position;
		vec4 up;
		vec4 forward;
		vec4 right;

		vec2 screenSize;
		float modifier;
		int modifier2;
		bool toggle;//TODO specialisation vars
	} camera;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

//UTILS

// polynomial smooth min (k = 0.1);
float smin( float a, float b, float k )
{
    float h = clamp( 0.5+0.5*(b-a)/k, 0.0, 1.0 );
    return mix( b, a, h ) - k*h*(1.0-h);
}

//DISTANCE FIELDS

float box(vec3 position, vec3 size)
{
	vec3 d = abs(position) - size;
	return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}
float plane(vec3 position, vec4 normal)
{
	// n must be normalized
	return dot(position, normal.xyz) + normal.w;
}
//SCENE
float scene(vec3 pos, inout int material)
{
	float cornerRad = 20.0;//larger = hole is rounder
	float size = 100;//overall size
	float subtractSize = camera.modifier;//size of subtracted cross
	int iterations = camera.modifier2;
	float base = box(pos,vec3(size));//base cube

	float currentSize = 1.0;
	for( int i = 0; i < iterations; i++ )
	{
		vec3 repeatP = mod( pos*currentSize, size*2.0 )-size;
		currentSize *= subtractSize;
		vec3 newP = abs(size - subtractSize*abs(repeatP));

		//subtract cross section (3 boxes in plane pairs)
		float bxy = max(newP.x,newP.y);
		float byz = max(newP.y,newP.z);
		float bzx = max(newP.z,newP.x);
		float new = (smin(bxy,smin(byz,bzx,cornerRad),cornerRad)-size)/currentSize;

		//max(new,base) and get material if hit
		if (new>base)
		{
			base = new;
			material = i;
		} 
	}

	/*//SLICE
	material = iterations;
	float sliceT = plane(pos, normalize(vec4(1,1,1,1)));
	if(base > sliceT)
		return base;
	else
		return sliceT;
	*/

	return base;
}

vec4 getMaterial(int material)
{
	//color based on unit digit
	switch(int(mod(material, 10)))
	{
	case 0:
		return vec4(0.95,0.11,0.15,1.0);
		break;
	case 1:
		return vec4(0.04,0.14,0.39,1.0);
		break;
	case 2:
		return vec4(0.98,0.85,0.20,1.0);
		break;
	case 3:
		return vec4(0.59,0.99,0.48,1.0);
		break;
	case 4:
		return vec4(0.44,0.78,0.75,1.0);
		break;
	case 5:
		return vec4(0.90,0.15,0.49, 1.0);
		break;
	case 6:
		return vec4(0.67,0.59,0.46, 1.0);
		break;
	case 7:
		return vec4(0.58,0.39,0.64, 1.0);
		break;
	case 8:
		return vec4(0.51,0.45,0.45, 1.0);
		break;
	case 9:
		return vec4(0.95,0.77,0.85, 1.0);
		break;
	default:
		return vec4(0.0, 0.0, 0.0, 1.0);
		break;
	};
}

vec3 getNormalAt(vec3 pos)
{
	vec3 n;
	float dt = 0.01;
	int dummy;//don't need material info from normal samples

	vec3 samplePoint = vec3(pos.x + dt, pos.y, pos.z);
	vec3 samplePoint2 = vec3(pos.x - dt, pos.y, pos.z);
	n.x = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	samplePoint = vec3(pos.x, pos.y + dt, pos.z);
	samplePoint2 = vec3(pos.x, pos.y - dt, pos.z);
	n.y = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	samplePoint = vec3(pos.x, pos.y, pos.z + dt);
	samplePoint2 = vec3(pos.x, pos.y, pos.z - dt);
	n.z = scene(samplePoint, dummy) - scene(samplePoint2, dummy); 

	return normalize(n);
}

float getShadow(vec3 pos, vec3 lightDir)
{
	int dummy;
	float shadow = 1.0;
	float maxdist = 500;//length(lightPos - pos);
	float dist = 0.1;
	float shadowHardness = 32;
	while(dist < maxdist)
	{
		float shadowPos = scene(pos + lightDir*dist, dummy);
		if( shadowPos < 0.01 )
		{
			shadow = 0.0;
			break;
		}
		shadow = min( shadow, shadowHardness*shadowPos/dist );
		dist += shadowPos;
	}

	return shadow;
}

vec4 shadeScene(vec3 pos, int material, vec4 lightColor)
{
	vec3 n = getNormalAt(pos);

	//apply material
	vec4 materialColor = getMaterial(material);
	vec4 finalColor = materialColor;

	//lighting
	vec3 lightDir = normalize(vec3(1.0, 0.5, .25));//directional sunlight
//	vec3 lightDir = normalize(vec3(15, 15, 15)- pos);
	vec4 intensity = lightColor * dot(n, lightDir);

	//compute shadows?
	if(camera.toggle)
	{
		float shadow = getShadow(pos, lightDir);

		finalColor *= intensity*shadow;
	}
	else finalColor *= intensity;

	return finalColor;
}

void main()
{
	//max loops
	int maxSteps = 512;
	
	//camera data
	vec3 eye = vec3(camera.position);
	vec3 right = vec3(camera.right);
	vec3 forward = vec3(camera.forward);
	vec3 up = vec3(camera.up);

	//shading
	vec4 lightColor = vec4(1.0,1.0,1.0, 1.0);
	int material;

	//map pixel coordinate to viewport
	float u = -1.0 + 2.0 * gl_FragCoord.x / camera.screenSize.x;
	float v = -1.0 + 2.0 * gl_FragCoord.y / camera.screenSize.y;
	//u *= camera.screenSize.x/camera.screenSize.y;

	//ray
	float f = 4.0;//focal length
	vec3 rayDirection = normalize(right * u + up * v + forward*f);

	//default colour (miss)
	vec4 color = vec4(0.3, 0.3, 0.3, 0.0);
	
	//distance travelled along the ray
	float raydist = 0.0;
	for(int i = 0; i < maxSteps; ++i)
	{
		//move along the ray
		vec3 position = eye + rayDirection * raydist;
		
		//check distance of position from surface
		float dist = scene(position, material);

		if(dist < 0.01)
		{
			//surface colour
			color = shadeScene(position, material, lightColor);
			break;
		}
		else if(raydist > 700) break;//line of sight

		raydist += dist;
	}

	outColor = color;
}
